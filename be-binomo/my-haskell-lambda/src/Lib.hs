{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module Lib where

import GHC.Generics
import Aws.Lambda
import Data.Aeson
import Data.List
import Data.Ord
import qualified Data.ByteString.Lazy.Char8 as ByteString

-- Input
data Event = Event
  { resource :: String
  , body :: String
  } deriving (Generic, FromJSON)

-- Output
data Response = Response
  { statusCode:: Int
  , body :: String
  } deriving (Generic, ToJSON)


-- Type that we decode from the 'body' of 'Event'
data Person = Person
  { name :: String
  , age :: Int
  } deriving (Generic, FromJSON, ToJSON)

greet :: Person -> String
greet person =
  "Hello, " ++ name person ++ "!"

data Project = Project
  { profit :: [Float]
  , initial :: Float
  , lifespan :: Float
  , discRate :: Float
  } deriving (Generic, FromJSON, ToJSON)


maxi xs = snd (maximumBy (comparing fst) (zip xs [0..]))

projectToNPV :: [Project] -> [Float]
projectToNPV [] = []
projectToNPV (x:xs) = (npv x) : projectToNPV xs

npv :: Project -> Float
npv project = npvHelper 1 (profit project) project

npvHelper :: Int -> [Float] -> Project -> Float
npvHelper _ [] project = 0
npvHelper n (x:xs) project = (pv n x project) + npvHelper (n+1) xs project

pv n profit project = (profit + (getDepretiation project)) / (1 / (1 + ((discRate project)^^n)))

getDepretiation :: Project -> Float
getDepretiation project = (initial project) / (lifespan project)

handler :: Event -> Context -> IO (Either String Response)
handler Event{..} context = do
  case decode (ByteString.pack body) of
    Just b ->
      pure $ Right Response
        { statusCode = 200
        , body = show (maxi (projectToNPV b))
        }
    Nothing ->
      pure $ Right Response
        { statusCode = 200
        , body = "Bakso"
        }
