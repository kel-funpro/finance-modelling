import React from "react";
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import './App.css';
import {NotFound} from './views';
import routes from "./routes";
// import { Container } from "react-bootstrap";
import Header from "./Header";
import Footer from "./Footer"

const App: React.FC = () => {

  return (
      <Router>
          <div>
              <Header />
              {/* <Container> */}
                  <Switch>
                      {routes.map(route => {
                          return <Route key={route.path} {...route}/>
                      })}
                      <Route component={()=><NotFound/>}/>
                  </Switch>
              {/* </Container> */}
              <Footer />
          </div>
      </Router>
  );
};

export default App;

