import React from 'react';
import '../App.css';

const BackgroundImagePage = () => {
    return (
        <div>
            <div className="bg-image">
                <div className="bg-text">
                        <h1 style={{fontSize: 50}}><strong>Selamat datang di Binomo!</strong></h1> 
                </div>
            </div>
                <br></br>
                <h4 style={{textAlign: "center"}}> Apa itu Binomo ? </h4>
                <br></br> 
                <Binomo/>
                <br></br>
                <h4 style={{textAlign: "center"}}> Bagaimana cara menjalankannya ? </h4>
                <br></br> 
                <CaraPakai/>
                <br></br> 
        </div>
   );
};

export default BackgroundImagePage
const Binomo = () => <div style={{border: "solid", textAlign: "center", width: "50%",
                                        display: "block", marginLeft: "auto", marginRight: "auto"}}>
    <p>Binomo adalah sebuah website yang digunakan untuk menghitung
        perkiraan jumlah saham yang akan kita miliki </p>
</div>

const CaraPakai = () => <div style={{border: "solid", textAlign: "center", width: "50%",
                                        display: "block", marginLeft: "auto", marginRight: "auto"}}>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
        ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
        ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
         mollit anim id est laborum. </p>
</div>
