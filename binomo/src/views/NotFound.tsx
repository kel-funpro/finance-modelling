import React from 'react';
import {Link} from "react-router-dom";
import PageNotFound from "../images/notfound.png"

export default () => {
    return (
          <>
              <img src={PageNotFound} style={{width: "70vw", display: 'block', margin: 'auto', position: 'relative' }} alt={"not found"}/>
              <Link to="/">
                  <h1>Go to home page</h1>
              </Link>
          </>
    );
};
