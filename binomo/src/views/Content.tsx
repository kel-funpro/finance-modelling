import React, {useState} from 'react';
import {Button} from "react-bootstrap";

export default () => {
    const [count, setCount] = useState(0);
    const handler = () => {
        if (count < 9){
            setCount(count + 1);
        } else {
            setCount(1);
            window.location.assign("/saham/form");
        }
        document.title = "Binomo - clicked "+(count+1)+" times";
    };
    return (
        <div>
            <h1>Hello, click 10 times</h1>
            <p>You clicked {count} times</p>
            <Button variant="primary" onClick={()=>handler()}>Click Me</Button>
        </div>
    );
};
