import React from 'react';
import '../App.css';
import { Card, Container, Row, Col } from 'react-bootstrap';
import bagas from '../images/bagas.png';
import luqi from '../images/luqi.png';
import selvy from '../images/selvy.png';
import rosyi from '../images/rosyi.png';
import mutia from '../images/mutia.png';


const AboutPage = () => {
    return (     
        <div>   
            <div className="bg-image">
                <div className="bg-text">
                    <h1 style={{fontSize: 50}}><strong>Meet our Team!</strong></h1>
                </div>  
            </div>

            <Container>
            <Row style={{paddingBottom: "50px"}}>
                
                <Col></Col>
                <Col>
                <div className="container mt-5">
                <Card style={{ width: '300' }}>
                    <Card.Img variant="top" src={bagas} alt="bridge"
                              style={{width:300,height:300}}/>
                        
                        <Card.Body>
                            <Card.Title><b>Priambudi L. Bagaskara</b></Card.Title>
                            <Card.Text>1606895171</Card.Text>
                        </Card.Body>

                    </Card>
                </div>
                </Col>

                <Col>
                <div className="container mt-5" style={{paddingLeft: "100px"}}>
                <Card style={{ width: '300' }}>
                    <Card.Img variant="top" src={luqi} alt="bridge"
                              style={{width:300,height:300}}/>
                        
                        <Card.Body>
                            <Card.Title><b>Khoirul Khuluqi A</b></Card.Title>
                            <Card.Text>1606833936</Card.Text>
                        </Card.Body>
                    </Card>
                </div>
                </Col>

                <Col></Col>

            </Row>
            </Container>
            <Container>
            <Row style={{paddingBottom: "50px"}}>
                
                <Col></Col>
                <Col>
                <div className="container mt-5">
                <Card style={{ width: '300' }}>
                    <Card.Img variant="top" src={mutia} alt="bridge"
                              style={{width:300,height:300}}/>
                        
                        <Card.Body>
                            <Card.Title><b>Mutia Rahmatun Husna</b></Card.Title>
                            <Card.Text>1706039622</Card.Text>
                        </Card.Body>

                    </Card>
                </div>
                </Col>

                <Col>
                <div className="container mt-5" style={{paddingLeft: "100px"}}>
                <Card style={{ width: '300' }}>
                    <Card.Img variant="top" src={selvy} alt="bridge"
                              style={{width:300,height:300}}/>
                        
                        <Card.Body>
                            <Card.Title><b>Selvy Fitriani</b></Card.Title>
                            <Card.Text>1706039446</Card.Text>
                        </Card.Body>
                    </Card>
                </div>
                </Col>

                <Col></Col>

            </Row>
            </Container>
            <Container>
            <Row style={{paddingBottom: "50px"}}>
                
                <Col></Col>
                <Col>
                <div className="container mt-5" style={{paddingLeft: "100px"}}>
                <Card style={{ width: '300' }}>
                    <Card.Img variant="top" src={rosyi} alt="bridge"
                              style={{width:300,height:300}}/>
                        
                        <Card.Body>
                            <Card.Title><b>Aulia Rosyida</b></Card.Title>
                            <Card.Text>1706025346</Card.Text>
                        </Card.Body>
                    </Card>
                </div>
                </Col>
            </Row>
            </Container>
        </div>
    );
};

export default AboutPage;
;