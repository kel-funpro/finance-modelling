import React from 'react';
import ProfilePicture from "./ProfilePicture";
import "../../App.css"
import {Col, Row} from "react-bootstrap";

export default () => {
    return (
        <section>
            <div className="text-center">
                <ProfilePicture
                    width={150}
                    height={150}
                    src={"https://images.askmen.com/1080x540/2016/01/25-021526-facebook_profile_picture_affects_chances_of_getting_hired.jpg"}
                />
                <h4 className={"hello-profile"}><span><strong>John Doe</strong></span></h4>
                <p className={"hello-info"}>john.doe@email.co</p>
                <Row>
                    <Col>
                        <p className={"hello-info"}>Profile 1</p>
                    </Col>
                    <Col>
                        <p className={"hello-info"}>Profile 2</p>
                    </Col>
                </Row>
            </div>
            <div>
                <h4 className={"history"}><span><strong>HISTORY</strong></span></h4>
                <div>

                </div>
            </div>
        </section>
    );
};
