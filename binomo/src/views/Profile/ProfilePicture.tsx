import React from 'react';

type ProfilePictureProps = {
    src: string,
    width: number,
    height: number,
}

export default ({src, width, height}: ProfilePictureProps) => {
    return (
        <img
            alt={""}
            width={width}
            height={height}
            style={{
                borderRadius: width/2,
                backgroundPosition: "center",
                backgroundImage: `url(${src})`,
                backgroundSize: "cover"}}
        />
    );
};
