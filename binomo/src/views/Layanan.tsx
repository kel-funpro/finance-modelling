import React from 'react';
import '../App.css';
import { Card, Button, Container, Row, Col } from 'react-bootstrap';
import jembatan from '../images/jembatan.png';


const Layanan = () => {
    return (     
        <div>   
            <div className="bg-image">
                <div className="bg-text">
                    <h1 style={{fontSize: 70}}><b>Layanan-layanan</b></h1>
                    <h1 style={{fontSize: 50}}><b>yang ada di Binomo</b></h1>
                </div>  
            </div>

            <Container>
            <Row style={{paddingBottom: "50px"}}>
                
                <Col></Col>
                <Col>
                <div className="container mt-5">
                <Card style={{ width: '300' }}>
                    <Card.Img variant="top" src={jembatan} alt="bridge"
                              style={{width:300,height:300}}/>
                        
                        <Card.Body>
                            <Card.Title><b>Investasi Saham Jembatan</b></Card.Title>
                            
                            <Container style={{paddingBottom: "20px"}}>
                                <Row>
                                    <Col><Card.Text>Persentase</Card.Text></Col>
                                    <Col><Card.Text>Uang</Card.Text></Col>
                                </Row>
                                <Row>
                                    <Col><Card.Text>5% Saham</Card.Text></Col>
                                    <Col><Card.Text>Rp15.567.900,00</Card.Text></Col>
                                </Row>
                            </Container>
                        </Card.Body>

                    </Card>
                </div>
                </Col>

                <Col>
                <div className="container mt-5" style={{paddingLeft: "100px"}}>
                <Card style={{ width: '300' }}>
                    <Card.Img variant="top" src={jembatan} alt="bridge"
                              style={{width:300,height:300}}/>
                        
                        <Card.Body>
                            <Card.Title><b>Investasi Saham Jembatan</b></Card.Title>
                            
                            <Container style={{paddingBottom: "20px"}}>
                                <Row>
                                    <Col><Card.Text>Persentase</Card.Text></Col>
                                    <Col><Card.Text>Uang</Card.Text></Col>
                                </Row>
                                <Row>
                                    <Col><Card.Text>5% Saham</Card.Text></Col>
                                    <Col><Card.Text>Rp15.567.900,00</Card.Text></Col>
                                </Row>
                            </Container>
                        </Card.Body>

                    </Card>
                </div>
                </Col>

                <Col></Col>

            </Row>
            </Container>

            <div className="container text-center" style={{paddingBottom: "100px"}}>
                <div>
                    <h1 style={{fontSize: 20}}><b>Perkiraan Saham</b></h1>
                    <br></br>
                    <Button variant="outline-dark" style={{padding: "15px"}} href="/form">
                        <b>HITUNGNYA DISINI YA</b>
                    </Button>
                </div>  
            </div>
        </div>

    );
};

export default Layanan;
