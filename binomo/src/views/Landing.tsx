import React from 'react';
import '../App.css';
import { Button } from 'react-bootstrap';

const LandingPage = () => {
    return (
        <div className="bg-image">
            <div className="bg-text">
                    <h1 style={{fontSize: 50}}><strong>Hello, I am Binomo!</strong></h1>
                    <Button variant="info" href="/Description"><strong>Predict My Invest</strong></Button> 
                </div>  
        </div>
   );
};

export default LandingPage;
