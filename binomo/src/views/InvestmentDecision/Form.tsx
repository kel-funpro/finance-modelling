import React, {useState} from "react";
import { Button, Form, Spinner } from "react-bootstrap";
import {
  InputError,
  ChildOfCol,
  InputForm,
  DropDownForm,
  listWithRange,
  handler,
  handlerWithError
} from "./FormUtils";
import Axios from "axios";
import {BackendURL} from "../../Axios/Endpoints";


export default () => {
  const blankForm = {
    portfolioType: "",
    initialAmount: "",
    projectLifespan: "",
    yearProfitState: "",
    depretiationMethod: "",
    discountRate: ""
  };

  const MAX_LIFESPAN = 20;
  const MAX_DISCOUNT = 100;

  const [formState, setFormState] = useState([{ ...blankForm }]);
  const [scoringTechniqueState, setScoringTechniqueState] = useState("");
  const [initialAmountState, setInitialAmountState] = useState("");
  const [projectLifespanState, setProjectLifespanState] = useState(0);
  const [isTooMuchLifespan, setIsTooMuchLifespan] = useState([false]);
  const [isTooMuchPercentage, setIsTooMuchPercentage] = useState([false]);
  const [yearProfitState, setYearProfitState] = useState([0]);
  const [depretiationMethod, setDepretiationMethod] = useState("");
  const [discountRateState, setDiscountRateState] = useState("");
  const [resultState, setResultState] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  // const temp = {
  //   scoringTechniqueState,
  //   initialAmountState,
  //   projectLifespanState,
  //   yearProfitState,
  //   depretiationMethod,
  //   discountRate: discountRateState,
  //   isTooMuchLifespan
  // };

  const print = (something: any) => console.log(something);

  const getProfitOfIndex = (index:number) => {
    let a = [];
    for (let i = 0; i<(projectLifespanState as any)["projectLifespan-"+index];i++){
      a.push(parseFloat((yearProfitState as any)['yearProfit-form'+index+'profit'+i]))
    }
    return a;
  };

  const getInitialOfIndex = (index:number) => {
    return parseFloat((initialAmountState as any)["initialAmount-"+index]);
  };

  const getLifespanOfIndex = (index:number) => {
    return parseFloat((projectLifespanState as any)["projectLifespan-"+index]);
  };

  const getDiscountRateOfIndex = (index:number) => {
    return parseFloat((discountRateState as any)["discountRate-"+index]);
  };

  let dataToSend = [] as any;

  const handleSubmit = (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();
    // print(temp);
    setIsLoading(true);
    for (let i = 0; i < formState.length; i++){
      const obj: { profit: number[], initial: number, lifespan: number, discRate: number} = {
        profit: getProfitOfIndex(i),
        initial: getInitialOfIndex(i),
        lifespan: getLifespanOfIndex(i),
        discRate: getDiscountRateOfIndex(i)
      };
      dataToSend.push(obj);
      
    }
    const headers = {
      'Content-Type': 'application/json',
    };
    Axios.post(BackendURL,
      JSON.stringify(dataToSend),
        {
          headers: headers,
        })
        .then((response)=>{
          setResultState(response.data);
        })
        .catch((error) => {
          return print(error);
        }).finally(()=>setIsLoading(false))
  };

  const addProject = () => {
    setFormState([...formState, { ...blankForm }]);
  };

  const pipe = (...fns:any) => (x:any) => fns.reduce((v:any, f:any) => f(v), x);

  const capitalise = (str:string) =>
    str.charAt(0).toLocaleUpperCase() + str.substr(1);

  const addEmptySpace = (string:string) => string+" ";

  const splitBy = (splitter:string) => (string:string) => string.split(splitter);

  const splitBySpace = splitBy(" ");

  const capitalizeEachWord = (list:string[]) => list.map(
    (item) => splitBySpace(item).map(
      pipe(
        addEmptySpace,
        capitalise,
      )
    )
  );

  const scoringTechniquesRaw = [
    "net present value",
    // "profitability index",
    // "payback period",
    // "average rate of return",
    // "internal rate of return"
  ];

  const scoringTechniques = capitalizeEachWord(scoringTechniquesRaw);

  const getTotalLifespanOfIndex = (i: number) => {
    const valueLifeSpan = parseInt(
      (projectLifespanState as any)["projectLifespan-" + i.toString()]
    );
    return valueLifeSpan > 0 ? valueLifeSpan : 0;
  };

  const initialAmount = (indexForm: number) =>
    InputForm(
      <Form.Control
        value={
          (initialAmountState as any)["initialAmount-" + indexForm] ===
          undefined
            ? ""
            : (initialAmountState as any)["initialAmount-" + indexForm]
        }
        name={"initialAmount-" + indexForm}
        type="number"
        placeholder="1000000000"
        onChange={(e: any) =>
          handler(e)(initialAmountState, setInitialAmountState)
        }
      />,
      false,
      "initialAmount",
      "Initial Amount",
      false,
      "Rp",
      ",00"
    );

  const projectLifespan = (indexForm: number) =>
    InputForm(
      <Form.Control
        name={"projectLifespan-" + indexForm}
        type="number"
        placeholder="6"
        onChange={(e: any) =>
          handlerWithError(e)(isTooMuchLifespan, setIsTooMuchLifespan)(
            projectLifespanState,
            setProjectLifespanState,
            e.target.value <= MAX_LIFESPAN
          )
        }
      />,
      <InputError
        condition={
          (isTooMuchLifespan as any)["projectLifespan-" + indexForm.toString()]
        }
        text={"Maximum lifespan is " + MAX_LIFESPAN + "!"}
      />,
      "projectLifespan",
      "Project Lifespan",
      false,
      false,
      "Years"
    );

  const discountRate = (indexForm: number) =>
    InputForm(
      <Form.Control
        name={"discountRate-" + indexForm}
        type="number"
        placeholder="30"
        onChange={(e: any) =>
          handlerWithError(e)(isTooMuchPercentage, setIsTooMuchPercentage)(
            discountRateState,
            setDiscountRateState,
            e.target.value <= MAX_DISCOUNT
          )
        }
      />,
      <InputError
        condition={
          (isTooMuchPercentage as any)["discountRate-" + indexForm.toString()]
        }
        text={"Maximum discount is " + MAX_DISCOUNT + "%!"}
      />,
      "discountRate",
      "Discount Rate",
      false,
      false,
      "%"
    );

    const SubmitButton = () => {
      return isLoading ? <Button variant="primary" disabled>
      <Spinner
        as="span"
        animation="grow"
        size="sm"
        role="status"
        aria-hidden="true"
      />
      Loading...
    </Button> : <Button className={"custom"} type="submit">
      Submit
    </Button>
    }

    const ordinalSuffixOf = (i:number) =>{
      let j = i % 10,
          k = i % 100;
      if (j === 1 && k !== 11) {
          return i + "st";
      }
      if (j === 2 && k !== 12) {
          return i + "nd";
      }
      if (j === 3 && k !== 13) {
          return i + "rd";
      }
      return i + "th";
  }

    const ResultText = (props: { projectNumber: any }) => {
    return props.projectNumber === "" ? props.projectNumber : <h1>Choose {ordinalSuffixOf(props.projectNumber+1)} Project.</h1>
    }

  return (
    <div style={{ margin: "50px", }}>
      <h1>Project Decision Maker</h1>
      <Form onSubmit={handleSubmit}>
        <DropDownForm
          id="formHorizontalEmail"
          name={"scoringTechnique"}
          label={"Scoring Technique"}
          value={(scoringTechniqueState as any)["scoringTechnique"]}
          length={5}
          onChange={e =>
            handler(e)(scoringTechniqueState, setScoringTechniqueState)
          }
        >
          {scoringTechniques.map((item:any, index:any) => {
            return (
              <option value={item} key={index}>
                {item}
              </option>
            );
          })}
        </DropDownForm>

        {formState.map((item, indexForm) => {
          return (
            <div className={"form-shell"} key={item + indexForm.toString()}>
              <h2>{"Project " + (indexForm + 1)}</h2>
              <Form.Row>
                <ChildOfCol child={initialAmount(indexForm)} />
                <ChildOfCol child={projectLifespan(indexForm)} />
              </Form.Row>
              {(isTooMuchLifespan as any)[
                "projectLifespan-" + indexForm.toString()
              ] ? (
                <></>
              ) : (
                listWithRange(getTotalLifespanOfIndex(indexForm)).map(
                  (_, indexProfit) => {
                    return InputForm(
                      <Form.Control
                        name={
                          "yearProfit-form" + indexForm + "profit" + indexProfit
                        }
                        type="number"
                        placeholder="100000"
                        onChange={(e: any) =>
                          handler(e)(yearProfitState, setYearProfitState)
                        }
                      />,
                      <InputError
                        condition={
                          (isTooMuchPercentage as any)[
                            "discountRate-" + indexForm.toString()
                          ]
                        }
                        text={"Maximum discount is " + MAX_DISCOUNT + "%!"}
                      />,
                      "yearProfit" + indexForm + indexProfit,
                      "Year " + (indexProfit + 1) + " Profit",
                      true,
                      "Rp",
                      ",00"
                    );
                  }
                )
              )}
              <Form.Row>
                <ChildOfCol
                  child={
                    <DropDownForm
                      id="depretiationMethod"
                      length={12}
                      label={"Depretiation Method"}
                      name={"depretiationMethod-" + indexForm}
                      value={
                        (depretiationMethod as any)[
                          "depretiationMethod-" + indexForm
                        ]
                      }
                      onChange={e =>
                        handler(e)(depretiationMethod, setDepretiationMethod)
                      }
                    >
                      <option value={"Straight Line"}>Straight Line</option>
                    </DropDownForm>
                  }
                />
                <ChildOfCol child={discountRate(indexForm)} />
              </Form.Row>
            </div>
          );
        })}
        <Button onClick={addProject} className={"custom"} variant="primary">
          Add New Project
        </Button>
        <SubmitButton />
      <ResultText projectNumber={resultState} />
      </Form>
    </div>
  );
};
