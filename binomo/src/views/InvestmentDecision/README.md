# Penjelasan beberapa fungsi dengan konsep Functional Programming

1. Pure Function - Form.tsx line 66
   
   Dapat dikatakan pure karena fungsi di atas mengembalikan hasil dari input yang diberikan.
   
   ```
   const addEmptySpace = (string:string) => string+" ";
   ```
   Fungsi di atas menerima sebuah parameter dan menambahkan space kosong di belakangnya. Berikut merupakan contoh Pure Function lainnya.

   ```
   const capitalise = (str:string) =>
    str.charAt(0).toLocaleUpperCase() + str.substr(1);
   ```
   Fungsi di atas menerima sebuah parameter dan menjadikan karakter pertamanya menjadi uppercase.

2. Pipes - Form.tsx line 61

    ```
    // fungsi pipe - Form.tsx  line 61
    const pipe = (...fns:any) => (x:any) => fns.reduce((v:any, f:any) => f(v), x);

    // implementasi pipe - Form.tsx  line 72
    const capitalizeEachWord = (list:string[]) => list.map(
        (item) => splitBySpace(item).map(
            pipe(
                addEmptySpace,
                capitalise,
            )
        )
    );
    ```

    Fungsi pipe di atas menerima 2 parameter, yang pertama adalah kumpulan fungsi - fungsi yang ingin dilakukan, dan yang kedua adalah objek sebagai argumen dari fungsi - fungsi sebelumnya. Kemudian fungsi - fungsi tersebut akan dijalankan dari atas ke bawah, kaena fungsi reduce pada JavaScript mengevaluasi dari depan ke belakang.

3. Point Free Style  - Form.tsx line 75-76
   Dikatakan Point Free Style karena fungsinya tidak mendeklarasikan argumen.

   ```
   [...]

    pipe(

        // implementasi Point Free Style - Form.tsx  line 75
        addEmptySpace,

        // implementasi Point Free Style - Form.tsx  line 76
        capitalise,
    )
    
    [...]
   ```
