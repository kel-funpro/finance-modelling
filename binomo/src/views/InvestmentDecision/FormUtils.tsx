import React from "react";
import { Col, Form, InputGroup, Row } from "react-bootstrap";

export const CurryError = (condition: boolean) => {
  return (text: string) =>
    condition ? <small style={{ color: "red" }}>{text}</small> : <></>;
};

export const InputError = ({
  condition,
  text
}: {
  condition: boolean;
  text: string;
}) => CurryError(condition)(text);

export const ChildOfCol = (props: { child: any }) => <Col>{props.child}</Col>;

export const Label = (props: { text: string; length: number }) => (
    <Form.Label column={true} sm={props.length}>
      {props.text}
    </Form.Label>
);

export const Prepend = (props: { text: string }) => (
  <InputGroup.Prepend>
    <InputGroup.Text>{props.text}</InputGroup.Text>
  </InputGroup.Prepend>
);

export const Append = (props: { text: string }) => (
  <InputGroup.Append>
    <InputGroup.Text>{props.text}</InputGroup.Text>
  </InputGroup.Append>
);

export const InputForm = (
  input: any,
  error: any,
  id: string,
  label: string,
  inline: boolean,
  prepend?: any,
  append?: any
) => {
  return (
    <Form.Group key={id} as={Row} controlId={id}>
    {label ? <Label text={label} length={inline ? 2 : 12} /> : <></>}
    <Col sm={inline ? 10 : 12}>
      <InputGroup className="mb-3">
        {prepend ? <Prepend text={prepend} /> : <></>}
        {input}
        {append ? <Append text={append} /> : <></>}
      </InputGroup>
      {error ? error : <></>}
    </Col>
  </Form.Group>
  )
}

export const DropDownForm = (props: {
  length: number;
  id: string;
  label: string;
  name: string;
  value: string;
  onChange: (a: any) => void;
  children: JSX.Element[] | JSX.Element;
}) => {
  let length = props.length || 12;
  return (
    <Form.Group as={Row} controlId={props.id}>
      <Form.Label column={true} sm={12}>
        {props.label}
      </Form.Label>
      <Col sm={length}>
        <DropDown
          name={props.name}
          value={props.value}
          onChange={props.onChange}
        >
          {props.children}
        </DropDown>
      </Col>
    </Form.Group>
  );
};

export const DropDown = (props: {
  name: string;
  value: string;
  onChange: (a: any) => void;
  children: JSX.Element[] | JSX.Element;
}) => (
  <Form.Control
    name={props.name}
    as="select"
    onChange={props.onChange}
    value={props.value}
  >
    <option>Options...</option>
    {props.children}
  </Form.Control>
);

export const listWithRange = (n: number) => {
  return [...Array(n)];
};

export const updateState = (e: React.FormEvent<EventTarget>) => {
  const target = e.target as any;
  return (state: any, setState: (a: any) => void, stateValue = target.value) =>
    setState({
      ...state,
      [target.name]: stateValue
    });
};

export const handler = (e: React.FormEvent<EventTarget>) => {
  e.preventDefault();
  return (state: any, setState: any) => updateState(e)(state, setState);
};

export const handlerWithError = (e: React.FormEvent<EventTarget>) => {
  e.preventDefault();
  return (
    errorState: any,
    errorSetState: (a: any) => void,
    defaultValue = false
  ) => {
    updateState(e)(errorState, errorSetState, defaultValue);
    return (state: any, setState: (a: any) => void, condition: boolean) => {
      if (condition) {
        updateState(e)(state, setState);
      } else {
        updateState(e)(errorState, errorSetState, !defaultValue);
      }
    };
  };
};
