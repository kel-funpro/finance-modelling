import React from "react";
import { Container, Row, Col, Navbar } from "react-bootstrap";
import Logo from "./images/logo.png";

function Footer () {
  return (
    <Navbar bg="info" variant="dark">
      <Container fluid className="text-center text-md-left">
        <Row>
          <Col md="6">
            <img
              src={Logo}
              width="150"
              height="150"
              className="d-inline-block align-top"
              alt="React Bootstrap logo"
            />
          </Col>
          <Col md="6">
            <br></br>
            <h5 className="title"><strong>BINOMO</strong></h5>
            <br></br>
                <p>Sebuah website yang diadaptasi dari rumus Monte Carlo</p>
          </Col>
        </Row>
      </Container>
      <Container fluid className="text-center text-md-left">
        <Row>
          <Col md="6">
            <h5 className="title"><strong>PROYEK AKHIR</strong></h5>
            <br></br>
                <p>Functional Programming-A</p>
          </Col>
        </Row>
      </Container>
      <Container fluid className="text-center text-md-left">
        <Row>
          <Col md="6">
            <br></br>
            <h5 className="title"><strong>Developer</strong></h5>
            <br></br>
                <p>Mutia Rahmatun Husna</p>
                <p>Aulia Rosyida</p>
                <p>Selvy Fitriani</p>
                <p>Priambudi Lintang Bagaskara</p>
                <p>Khoirul Khuluqi Abdullah</p>
          </Col>
        </Row>
      </Container>
    </Navbar>
  );
}

export default Footer;