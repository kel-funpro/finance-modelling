import Axios from "axios";
import {BackendURL} from "./Endpoints";

export const GetService = () => {
    Axios.get(BackendURL)
        .then((response)=>{
            return response;
        })
        .catch((error) => {
            return error;
        })
};

export const PostService = (obj:any) => {
    const headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials' : true,
    };
    Axios.post(BackendURL,
        {obj},
        {
            headers
        })
        .then((response)=>{
            return response;
        })
        .catch((error) => {
            return error;
        })
};
