import { FormSaham, Description, Output, AboutUs, Landing, Layanan} from "../views"
import Profile from "../views/Profile/Profile";

const routes = [
    {
        path: "/",
        component: Landing,
        exact: true,
        default: false,
    },
    {
        path: "/form",
        component: FormSaham,
        exact: true,
        default: false,
    },
    {
        path: "/description",
        component: Description,
        exact: true,
        default: false,
    },
    {
        path: "/profile",
        component: Profile,
        exact: true,
        default: false,
    },
    {
        path: "/output",
        component: Output,
        exact: true,
        default: false,
    },
    {
        path: "/about-us",
        component: AboutUs,
        exact: true,
        default: false,
    },
    {
        path: "/layanan",
        component: Layanan,
        exact: true,
        default: false,
    },
];


export default routes;
