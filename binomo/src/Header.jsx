import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import Logo from "./images/logo.png"

const Production = "http://binomofp.s3-website-ap-southeast-1.amazonaws.com";

const Localhost = "http://localhost:3000"

const Home = (props) => {
  if (props.isLanding || props.isDescription || props.isLayanan || props.isLogin || props.isSignUp){
    return <Nav.Link href="/"><strong>HOME</strong></Nav.Link>
  } else return <></>
}

const Layanan = (props) => {
  if (props.isDescription || props.isLayanan || props.isLogin || props.isSignUp){
    return <Nav.Link href="/layanan"><strong>LAYANAN</strong></Nav.Link>
  } else return <></>
}

const About = (props) => {
  if (props.isLanding || props.isLayanan || props.isDescription){
    return <Nav.Link href="/about-us"><strong>ABOUT US</strong></Nav.Link>
  } else return <></>
}

const SignUp = (props) => {
  if (props.isDescription || props.isLayanan || props.isLogin) {
    return <Nav.Link href="/signup"><strong>SIGN UP</strong></Nav.Link>
  } else return <></>
}

const Login = (props) => {
  if (props.isDescription || props.isLayanan || props.isSignUp) {
    return <Nav.Link href="/login"><strong>LOGIN</strong></Nav.Link>
  } else return <></>
}

function Header() {
  return (
    <Navbar bg="info" variant="dark">
    <Navbar.Brand>
      <img
        src={Logo}
        width="50"
        height="50"
        className="d-inline-block align-top"
        alt="React Bootstrap logo"
      />
    </Navbar.Brand>
    <Nav className="mr-auto">
      <Home
        isLanding={window.location.href===Production+"/" || Localhost+"/"}
        isLayanan={window.location.href===Production + "/layanan" || Localhost +"/Layanan"}
        isDescription={window.location.href===Production + "/Description" || Localhost + "/Description"}
        isLogin={window.location.href===Production + "/login" || Localhost + "/login"}
        isSignUp={window.location.href===Production + "/signup" || Localhost + "/signup"}
      />
      <Layanan
        isDescription={window.location.href===Production + "/Description" || Localhost + "/Description"}
        isLayanan={window.location.href===Production + "/layanan" || Localhost + "/layanan"}
        isLogin={window.location.href===Production + "/login" || Localhost + "/login"}
        isSignUp={window.location.href===Production + "/signup" || Localhost + "/signup"}
      />
      <About
        isLanding={window.location.href===Production + "/" || Localhost + "/"}
        isLayanan={window.location.href===Production + "/layanan" || Localhost + "/layanan"}
        isDescription={window.location.href===Production + "/Description" || Localhost + "/Description"}
      />
    </Nav>
    <Nav className="justify-content-end" activeKey="/home">
      <SignUp
        isDescription={window.location.href===Production + "/Description" || Localhost + "/Description"}
        isLayanan={window.location.href===Production + "/layanan" || Localhost + "/layanan"}
        isLogin={window.location.href===Production + "/login" || Localhost + "/login"}
      />
      <Login
        isDescription={window.location.href===Production + "/Description" || Localhost + "/Description"}
        isLayanan={window.location.href===Production + "/layanan" || Localhost + "/layanan"}
        isSignUp={window.location.href===Production + "/signup" || Localhost + "/layanan"}
      />
  </Nav>
  </Navbar>
  );
}
export default Header;
