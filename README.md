# binomo
Kelompok 3 Functional Programming 2019/2020

Kelas: Pemrograman Fungsional kelas A

Anggota kelompok:

1. Mutia Rahmatun Husna - 1706039622

2. Aulia Rosyida - 1706025346

3. Selvy Fitriani - 1706039446

4. Khoirul Khuluqi A - 1606833936

5. Priambudi Lintang Bagaskara - 1606895171

### Prerequisites

Sebelum memulai mendevelop, pastikan sudah memiliki [node](https://nodejs.org/en/) dan `node package manager`. Cek instalasi dengan menjalankan command di bawah ini.

```
node -v
npm -v
```

### Installing Frontend

pindah ke directory binomo
```
# windows, linux, apple
cd binomo
```

Setelah masuk ke dalam directory, jalankan command di bawah.

```
npm install
npm start
```

`npm install` bekerja seperti `pip install -r requirements.txt`, dia menginstall semua package (jika belum terinstall di `node_modules`)yang didaftarkan di `package.json`.

Secara default website dapat dibuka di [localhost:3000](http://localhost:3000)
